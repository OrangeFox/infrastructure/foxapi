from pydantic import BaseModel

GLOBAL_STATS_AGGREGATION = [
    {
        '$project': {
            'count': 1,
            'device_id': 1,
            'release_id': 1,
            # 'mirrors': 1,
            'dayEntries': {
                '$objectToArray': '$days'
            }
        }
    }, {
        '$unwind': '$dayEntries'
    }, {
        '$group': {
            '_id': {
                'day': '$dayEntries.k'
            },
            'dayValue': {
                '$sum': '$dayEntries.v'
            },
            'count': {
                '$sum': '$count'
            },
            'uniqueDevices': {
                '$addToSet': '$device_id'
            },
            'uniqueReleases': {
                '$addToSet': '$release_id'
            },
            # 'allMirrors': {
            #     '$push': '$mirrors'
            # }
        }
    }, {
        '$group': {
            '_id': None,
            'days': {
                '$push': {
                    'k': '$_id.day',
                    'v': '$dayValue'
                }
            },
            'count': {
                '$first': '$count'
            },
            'uniqueDevices': {
                '$first': '$uniqueDevices'
            },
            'uniqueReleases': {
                '$first': '$uniqueReleases'
            },
            # 'mirrors': {
            #     '$first': '$allMirrors'
            # }
        }
    }, {
        '$project': {
            '_id': 0,
            'count': 1,
            'deviceCount': {
                '$size': '$uniqueDevices'
            },
            'releaseCount': {
                '$size': '$uniqueReleases'
            },
            'days': {
                '$arrayToObject': '$days'
            },
            # 'mirrors': {
            #     '$arrayToObject': {
            #         '$objectToArray': {
            #             '$mergeObjects': '$mirrors'
            #         }
            #     }
            # }
        }
    }
]


class GlobalStatsAggregationModel(BaseModel):
    count: int
    deviceCount: int
    releaseCount: int
    days: dict[str, int]
    # mirrors: dict[str, list[str]]
