from typing import Dict

from beanie import Link, Document

from src.models.device import Device
from src.models.releasemodel import ReleaseModel
from src.utils.object_id import ObjectId


class ReleaseStats(Document):
    device_id: Link[Device]
    release_id: Link[ReleaseModel]
    count: int
    days: Dict[str, int]
    mirrors: Dict[str, int]

    class Settings:
        name = "stats"

    @staticmethod
    async def get_by_release_id(release_id: ObjectId) -> "ReleaseStats":
        return await ReleaseStats.find_one(ReleaseStats.release_id == release_id)

    @staticmethod
    async def get_by_device_id(device_id: ObjectId) -> list["ReleaseStats"]:
        return await ReleaseStats.find(ReleaseStats.device_id == device_id).to_list()
