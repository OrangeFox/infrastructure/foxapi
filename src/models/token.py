from datetime import datetime

from beanie import Document, Link

from src.models.user import User


class Token(Document):
    user_id: Link[User]
    username: str
    iat: datetime
    exp: datetime
    token: str

    class Settings:
        name = "tokens"
