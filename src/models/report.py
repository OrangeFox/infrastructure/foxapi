from datetime import datetime
from typing import List, Dict

from beanie import Document, Link
from pydantic import Field

from src.models.releasemodel import ReleaseModel


class Report(Document):
    release_id: Link[ReleaseModel]
    date: datetime = Field(default_factory=datetime.now)

    details: str
    logs: Dict[str, str]

    screenshots: List[bytes]

    class Settings:
        name = "reports"
