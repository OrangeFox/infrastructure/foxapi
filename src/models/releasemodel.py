from datetime import datetime
from enum import Enum
from typing import List, Optional

from beanie import Document
from pydantic import BaseModel

from src.utils.object_id import ObjectId


class ReleaseType(Enum):
    stable = 'stable'
    beta = 'beta'
    alpha = 'alpha'


class RecoveryImg(BaseModel):
    size: int
    md5: str


class ReleaseInternal(BaseModel):
    prop: Optional[dict[str, str | int | bool]] = None
    # released_by: Optional[Link[User]]


class ReleaseModel(Document):
    device_id: ObjectId
    date: datetime

    type: ReleaseType
    version: str

    changelog: List[str]
    bugs: Optional[List[str]]
    notes: Optional[str]

    variant: Optional[str] = None

    filename: str
    md5: str

    freezed: bool

    size: int
    recovery_img: RecoveryImg
    internal: ReleaseInternal

    archived: bool = False
    archived: bool = False

    class Settings:
        name = "releases"
