from datetime import datetime
from typing import Any

from beanie import Document, Link
from pydantic import Field

from src.models.user import User
from src.utils.object_id import ObjectId


class LoggingModel(Document):
    user: Link[User]
    timestamp: datetime = Field(default_factory=datetime.utcnow)  # Renamed from datetime

    action: str

    ip: str | None = None  # For auth endpoints
    device_id: ObjectId | None = None
    release_id: ObjectId | None = None
    user_id: ObjectId | None = None

    data: dict[str, Any] | None = None

    @staticmethod
    async def add_to_log(user: ObjectId, action: str, **data: dict[str, str]):
        model = LoggingModel(user=user, action=action, **data)
        await model.insert()
