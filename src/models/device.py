from typing import List, Optional, Any, Dict

import pymongo
from beanie import Document, Link
from beanie.odm.operators.find.comparison import In
from beanie.odm.operators.find.logical import Or
from pydantic import BaseModel, model_validator

from src.models.user import User


class DeviceTree(BaseModel):
    id: int
    name: Optional[str] = ""
    url: Optional[str] = ""


class Device(Document):
    # Old fields
    codename: str
    model_name: str
    oem_name: str

    # New fields
    codenames: List[str]
    model_names: List[str]

    supported: bool
    maintainer: Link[User]

    gitlab: Optional[DeviceTree] = None

    notes: Optional[str] = None
    freezed: bool

    @model_validator(mode='before')
    @classmethod
    def create_new_fields_if_not_exists(cls, data: Dict[str, Any]) -> Dict[str, Any]:
        data['codenames'] = data.get('codenames', [data.get('codename')])
        data['model_names'] = data.get('model_names', [data.get("model_name")])
        return data

    class Settings:
        name = "devices"
        indexes = [
            "codename",
            [
                ("codename", pymongo.DESCENDING),
            ],
        ]

    @staticmethod
    async def get_by_codename(codename: str) -> Optional["Device"]:
        return await Device.find_one(
            Or(
                Device.codename == codename,
                In(Device.codenames, [codename])
            )
        )
