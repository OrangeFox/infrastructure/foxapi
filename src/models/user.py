from enum import Enum
from typing import List, Optional

import pymongo
from beanie import Document
from pydantic import BaseModel


class Roles(Enum):
    maintainer = 'maintainer'
    admin = 'admin'
    manual_upload = 'manual_upload'
    releasing_stable = 'releasing_stable'
    stats = 'stats'
    danger_tasks = 'danger_tasks'
    create_new_devices = 'create_new_devices'
    mail = 'mail'


class UserTelegram(BaseModel):
    id: int


class UserGitlab(BaseModel):
    id: int


class UserMail(BaseModel):
    username: str


class User(Document):
    username: str
    name: str
    salt: Optional[bytes] = None
    key: Optional[bytes] = None
    roles: List[str]
    freezed: bool = False

    telegram: Optional[UserTelegram] = None
    gitlab: Optional[UserGitlab] = None
    mail: Optional[UserMail] = None
    admin_notes: Optional[str] = None

    class Settings:
        name = "users"
        indexes = [
            "username",
            [
                ("username", pymongo.DESCENDING),
            ],
        ]
