from asyncio import sleep
from datetime import datetime, timezone
from random import uniform
from typing import AsyncGenerator

from fastapi import Request

from src.utils.logger import log


async def randomized_response_time(
        request: Request,
        min_time: float = 0.3,
        max_time: float = 1
) -> AsyncGenerator[None, None]:
    """
    FastAPI dependency that adds a randomized delay to endpoint responses
    to ensure they take at least min_time and at most max_time seconds.

    Usage:
        @app.get("/some-endpoint")
        async def endpoint(delay: None = Depends(randomized_response_time)):
            return {"data": "response"}

        Or with custom timing:
            def custom_timing():
                return randomized_response_time(min_time=0.5, max_time=1.0)

            @app.get("/slower-endpoint")
            async def slower_endpoint(delay: None = Depends(custom_timing)):
                return {"data": "slow response"}
    """
    start_time = datetime.now(timezone.utc)
    log.info(f"Request started at {start_time.isoformat()} for path: {request.url.path}")
    try:
        yield
    finally:
        elapsed = (datetime.now(timezone.utc) - start_time).total_seconds()
        target_time = uniform(min_time, max_time)
        remaining_time = target_time - elapsed
        log.info(f"Elapsed time: {elapsed:.4f} seconds")
        log.info(f"Target time: {target_time:.4f} seconds")
        if remaining_time > 0:
            log.info(f"Sleeping for {remaining_time:.4f} seconds")
            await sleep(remaining_time)
        else:
            log.info("No additional sleep needed")
