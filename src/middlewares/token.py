from datetime import UTC, datetime

from fastapi import Request, HTTPException

from src.models.token import Token


async def require_token(request: Request) -> Token:
    raw_token = request.headers.get('Authorization')

    # Check if token is provided
    if not raw_token:
        raise HTTPException(status_code=401, detail="Token is missing")

    time_now = datetime.now(tz=UTC)
    token = await Token.find_one(Token.token == raw_token)

    if not token:
        raise HTTPException(status_code=401, detail="Invalid token")

    expire_at = token.exp.replace(tzinfo=UTC)

    if expire_at < time_now:
        await token.delete()
        raise HTTPException(status_code=401, detail="Token is expired")

    return token
