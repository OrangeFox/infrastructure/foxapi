from fastapi import Request, HTTPException

from src.middlewares.token import require_token
from src.models.token import Token
from src.models.user import Roles
from src.utils.logger import log


def require_role(*required_roles: Roles):
    async def wrapper(request: Request):
        token: Token = await require_token(request)
        user = await token.user_id.fetch()
        user_roles = user.roles
        log.debug(f"User roles: {user_roles}")
        log.debug(f"Required roles: {required_roles}")

        if any(role.value not in user_roles for role in required_roles):
            raise HTTPException(status_code=403, detail="Forbidden")

        return {'roles': user.roles, 'user_id': user.id}

    return wrapper
