import uvicorn
from fastapi import FastAPI
from slowapi import _rate_limit_exceeded_handler
from slowapi.errors import RateLimitExceeded
from slowapi.middleware import SlowAPIMiddleware
from starlette.middleware.cors import CORSMiddleware

from src import limiter
from src.routers import include_routers
from src.settings import SETTINGS
from src.utils.db import init_db

description = """
Warning: Please add a custom 'X-FoxAPI-App=MyAppName' header to your requests.
This would help us fighting against DDoS attacks in future, while keeping your application's access to the API.
In future, this may be a mandatory requirement.

To reduce the system load, the API endpoints are rate limited.
The default limit is 20 requests per minute. Contact admin@orangefox.tech if you need a higher limit.
"""

app = FastAPI(
    title='Fox API',
    summary='OrangeFox Recovery REST API',
    description=description,
    version='5.0.0',
    contact={
        'email': 'admin@orangefox.tech'
    }
)

app.state.limiter = limiter
app.add_exception_handler(RateLimitExceeded, _rate_limit_exceeded_handler)

include_routers(app, SETTINGS.internal)

app.add_middleware(SlowAPIMiddleware)

app.add_middleware(
    CORSMiddleware,
    allow_origins=SETTINGS.internal_cors_origins if SETTINGS.internal else ['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def app_startup():
    await init_db()


if __name__ == "__main__":
    uvicorn.run("__main__:app", host="0.0.0.0", port=5000, log_level="info", forwarded_allow_ips="*")
