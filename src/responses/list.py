from typing import Generic, TypeVar, List

from pydantic import BaseModel, computed_field

DataT = TypeVar('DataT')


class ListResponse(BaseModel, Generic[DataT]):
    data: List[DataT]

    @computed_field
    @property
    def count(self) -> int:
        return len(self.data)

    @staticmethod
    def new(data: DataT):
        return ListResponse(data=data)
