from typing import List, Optional

from pydantic import BaseModel, Field, computed_field

from src.models.device import Device
from src.models.user import User
from src.settings import SETTINGS
from src.utils.object_id import ObjectId


class DeviceMaintainer(BaseModel):
    legacy_id: ObjectId = Field(
        alias='id',
        serialization_alias='_id',
        deprecated=True
    )
    id: ObjectId
    name: str
    username: str

    class Config:
        fields = {'id': '_id'}
        schema_extra = {
            "examples": [{
                "_id": "5e5173120ced0d37269f8228",
                "id": "5e5173120ced0d37269f8228",
                "name": "Yacha",
                "username": "yacha",
            }]
        }


class ShortDeviceResponse(BaseModel):
    # Deprecated fields
    legacy_id: ObjectId = Field(
        alias='id',
        serialization_alias='_id',
        deprecated=True
    )
    codename: str = Field(
        description="Device codename, legacy",
        deprecated=True
    )
    model_name: str = Field(
        description="Model name of device, does not include the OEM part, legacy",
        deprecated=True
    )
    oem_name: str

    id: ObjectId
    codenames: List[str]
    model_names: List[str]
    supported: bool

    @computed_field(
        description="Full name of device / devices, legacy",
        # deprecated=True
    )
    @property
    def full_name(self) -> str:
        return f"{self.oem_name} {self.model_name}"

    @computed_field(description="URL of the device on the official website")
    @property
    def url(self) -> str:
        return f"{SETTINGS.release_url}/device/{self.id}"

    model_config = {
        "json_schema_extra": {
            "examples": [{
                "_id": "6574e52f7f9618c5be6930ab",
                "id": "6574e52f7f9618c5be6930ab",
                "codename": "mondrian",
                "model_name": "Poco F5 Pro",
                "oem_name": "Xiaomi",
                "codenames": ["mondrian"],
                "model_names": ["Poco F5 Pro", "Redmi K60"],
                "supported": True,
                "full_name": "Xiaomi Poco F5 Pro",
                "url": "https://orangefox.download/device/6574e52f7f9618c5be6930ab"
            }]
        }
    }


class DeviceResponse(ShortDeviceResponse):
    maintainer: DeviceMaintainer
    notes: Optional[str] = None
    device_tree: Optional[str] = None

    @staticmethod
    async def from_db(model: Device):
        maintainer = await User.get(model.maintainer.ref.id)
        if not maintainer:
            maintainer = await User.get(ObjectId(SETTINGS.nobody_id))

        return DeviceResponse(
            **model.model_dump(exclude={'maintainer'}),
            device_tree=model.gitlab.url if model.gitlab else None,
            maintainer=DeviceMaintainer(**maintainer.dict())
        )

    model_config = {
        "json_schema_extra": {
            "examples": [ShortDeviceResponse.model_config['json_schema_extra']['examples'][0] | {
                "maintainer": DeviceMaintainer.Config.schema_extra['examples'][0],
                "notes": "Public notes here",
                "device_tree": "https://gitlab.com/OrangeFox/device/mondrian",
            }]
        }
    }
