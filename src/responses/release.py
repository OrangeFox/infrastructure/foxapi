from datetime import datetime
from typing import List, Optional, Dict, Self
from uuid import UUID

from pydantic import BaseModel, field_serializer, Field, computed_field

from src.models.releasemodel import ReleaseType, ReleaseModel
from src.settings import SETTINGS
from src.utils.object_id import ObjectId


class RecoveryImgResponse(BaseModel):
    size: int
    md5: str

    model_config = {
        "json_schema_extra": {
            "examples": [{
                "size": 201326592,
                "md5": "22f496c01230c45082b6dbfbd1f1588e"
            }]
        }
    }


class ShortReleaseResponse(BaseModel):
    # Deprecated fields
    legacy_id: ObjectId = Field(
        alias='id',
        serialization_alias='_id',
        deprecated=True
    )
    size: int = Field(
        deprecated=True
    )

    id: ObjectId = Field(
        description="Release ID, that's how we indentify the release in the infrastructure",
    )
    build_id: Optional[UUID] = Field(
        None,
        description="Build ID, accessible also via `ro.build.fox_id` prop"
    )
    device_id: ObjectId
    date: datetime
    md5: str
    version: str
    type: ReleaseType

    archived: bool

    @field_serializer('date')
    def serialize_dt(self, date: datetime, _info):
        return date.timestamp()

    model_config = {
        "json_schema_extra": {
            "examples": [{
                "_id": "65f9ba1decb3b5ddc03bb83c",
                "id": "65f9ba1decb3b5ddc03bb83c",
                "build_id": "039e5b07-5e48-43ed-9846-7ee64791356e",
                "device_id": "626cf5cc6a44bc738419906f",
                "size": 75859407,
                "date": 1710864925,
                "md5": "c5d09519d00eed9ae7602bfcd26a6110",
                "version": "R11.1_5",
                "type": "stable"
            }]
        }
    }

    @staticmethod
    def from_db(model: ReleaseModel) -> "Self":
        return {
            **model.model_dump(),
            'build_id': model.internal.prop.get('ro,build,fox_id') if model.internal.prop else None,
            'device_id': model.device_id,
        }

    @staticmethod
    def from_db_list(models: List[ReleaseModel]) -> List["Self"]:
        return [ShortReleaseResponse.from_db(model) for model in models]


class ReleaseResponse(ShortReleaseResponse):
    recovery_img: RecoveryImgResponse

    changelog: List[str]
    bugs: Optional[List[str]]
    notes: Optional[str]

    filename: str

    @computed_field(description="URL of the release on the official website")
    @property
    def url(self) -> str:
        return f"{SETTINGS.release_url}/release/{self.id}"

    @computed_field(
        description="Mirrors list (deprecated)",
        # deprecated=True
    )
    @property
    def mirrors(self) -> Dict[str, str]:
        if self.archived:
            return {'AR': f"{SETTINGS.mirror_url}/{self.id}"}
        return {'DL': f"{SETTINGS.mirror_url}/{self.id}"}

    model_config = {
        "json_schema_extra": {
            "examples": [ShortReleaseResponse.model_config['json_schema_extra']['examples'][0] | {
                "changelog": [
                    "First change",
                    "Second change"
                ],
                "bugs": None,
                "notes": "Optional public release notes",
                "filename": "OrangeFox-alioth-stable@R11.1_5.zip",
                "url": "https://orangefox.download/release/65f9ba1decb3b5ddc03bb83c",
                "mirrors": {
                    "DL": "https://dl.orangefox.download/65f9ba1decb3b5ddc03bb83c"
                }
            }]
        }
    }
