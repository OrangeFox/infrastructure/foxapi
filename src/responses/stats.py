from typing import Dict

from pydantic import BaseModel

from src.models.stats import ReleaseStats
from src.utils.merge_dicts import merge_and_sum_dicts


class ReleaseStatsResponse(BaseModel):
    count: int
    days: Dict[str, int]
    mirrors: Dict[str, int]


class DeviceStatsResponse(BaseModel):
    count: int
    days: Dict[str, int]
    mirrors: Dict[str, int]

    @staticmethod
    def from_releases_stats(items: list[ReleaseStats]):
        return DeviceStatsResponse(
            count=sum(x.count for x in items),
            days=merge_and_sum_dicts(x.days for x in items),
            mirrors=merge_and_sum_dicts(x.mirrors for x in items),
        )
