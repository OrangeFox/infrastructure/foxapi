from typing import Optional

from src.models.device import Device
from src.models.stats import ReleaseStats
from src.responses.device import DeviceResponse
from src.responses.stats import DeviceStatsResponse


class DeviceInternalResponse(DeviceResponse):
    stats: Optional[DeviceStatsResponse]
    freezed: bool

    @staticmethod
    async def from_db(model: Device, stats: Optional[ReleaseStats]) -> "DeviceInternalResponse":
        super_values = (await DeviceResponse.from_db(model)).dict()
        return DeviceInternalResponse(
            **super_values,
            **model.model_dump(exclude=super_values.keys()),
            stats=stats
        )
