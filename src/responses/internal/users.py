from typing import Optional

from pydantic import BaseModel

from src.models.user import UserMail, UserGitlab, UserTelegram
from src.responses.device import ShortDeviceResponse
from src.utils.object_id import ObjectId


class UserShortResponse(BaseModel):
    id: ObjectId
    username: str
    name: str
    freezed: bool


class UserInternalResponse(UserShortResponse):
    roles: list[str]
    telegram: Optional[UserTelegram] = None
    gitlab: Optional[UserGitlab] = None
    mail: Optional[UserMail] = None
    admin_notes: Optional[str] = None

    maintained_devices: list[ShortDeviceResponse]
