from typing import Optional, Self

from src.models.releasemodel import ReleaseModel
from src.models.stats import ReleaseStats
from src.responses.release import ReleaseResponse
from src.responses.stats import ReleaseStatsResponse


class ReleaseInternalResponse(ReleaseResponse):
    prop: Optional[dict[str, str | int | bool]] = None
    stats: Optional[ReleaseStatsResponse] = None

    model_config = {
        "json_schema_extra": {
            "examples": [ReleaseResponse.model_config['json_schema_extra']['examples'][0] | {
                "prop": {
                    "ro,build,fox_id": "039e5b07-5e48-43ed-9846-7ee64791356e"
                }
            }]
        }
    }

    @staticmethod
    def from_db(model: ReleaseModel, stats: Optional[ReleaseStats] = None) -> "Self":
        return {
            **model.model_dump(),
            'stats': stats,
            'prop': model.internal.prop or None,
            'build_id': model.internal.prop.get('ro,build,fox_id', None) if model.internal.prop else None,
            'device_id': model.device_id,
        }
