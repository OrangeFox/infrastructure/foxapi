import secrets
from pathlib import Path

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    secret: str = secrets.token_urlsafe(32)
    mongodb_uri: str = 'mongodb://localhost'

    mirror_country: str = 'DE'
    mirror_url: str = 'https://dl.orangefox.download'
    release_url: str = 'https://orangefox.download'

    nobody_id: str = '5ff88a1eaaa6d11ee93f21ae'

    internal: bool = True
    token_expire: int = 5 * 60  # 5 minutes
    files_path: Path = '/home/yacha/OrangeFox-Releases'

    internal_cors_origins: list[str] = [
        'http://localhost:3000',
        'http://127.0.0.1:3000',
        'https://foxbox.orangefox.download'
    ]


SETTINGS = Settings()
