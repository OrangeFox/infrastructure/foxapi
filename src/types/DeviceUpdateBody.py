from pydantic import BaseModel


class DeviceUpdateBody(BaseModel):
    oem_name: str | None = None
    codenames: list[str] | None = None
    model_names: list[str] | None = None
    supported: bool | None = None
    freezed: bool | None = None
    notes: str | None = None
