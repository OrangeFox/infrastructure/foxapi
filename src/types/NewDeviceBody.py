from pydantic import BaseModel

from src.utils.object_id import ObjectId


class NewDeviceBody(BaseModel):
    oem_name: str
    codenames: list[str]
    model_names: list[str]
    maintainer: ObjectId
