from pydantic import BaseModel


class UserUpdateBody(BaseModel):
    username: str | None = None
    name: str | None = None
    freezed: bool | None = None
    roles: list[str] | None = None
    admin_notes: str | None = None
