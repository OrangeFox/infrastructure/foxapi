from pydantic import BaseModel


class ReleaseUpdateBody(BaseModel):
    changelog: list[str] | None = None
    bugs: list[str] | None = None
    notes: str | None = None
    freezed: bool | None = None
