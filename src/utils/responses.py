responses = {
    400: {"description": "Bad request"},
    404: {"description": "Item not found"},
    429: {"description": "Too many requests"},
}
