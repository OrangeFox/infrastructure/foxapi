from beanie import init_beanie
from motor.motor_asyncio import AsyncIOMotorClient

from src.models.device import Device
from src.models.logging import LoggingModel
from src.models.releasemodel import ReleaseModel
from src.models.report import Report
from src.models.stats import ReleaseStats
from src.models.token import Token
from src.models.user import User
from src.settings import SETTINGS

db = AsyncIOMotorClient(SETTINGS.mongodb_uri)


async def init_db():
    await init_beanie(database=db.general,
                      document_models=[User, Token, Device, ReleaseModel, Report, ReleaseStats, LoggingModel])
