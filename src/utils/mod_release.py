import os
from pathlib import Path

from src.models.releasemodel import ReleaseModel
from src.settings import SETTINGS
from src.utils.object_id import ObjectId


async def delete_release(release_id: ObjectId) -> bool:
    """Ensures that release is deleted"""
    real_filename = Path(f'{release_id}.zip')
    real_path = Path(SETTINGS.files_path / real_filename)
    if real_path.exists():
        os.remove(real_path)

    release = await ReleaseModel.get(release_id)
    if release:
        await release.delete()

    return True
