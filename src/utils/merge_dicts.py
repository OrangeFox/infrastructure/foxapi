from functools import reduce


def merge_and_sum_dicts(dict_list):
    def merge_dicts(d1, d2):
        merged = {**d1}  # Create a copy of the first dictionary
        for key, value in d2.items():
            if isinstance(value, int):
                merged[key] = merged.get(key, 0) + value
            else:
                merged.setdefault(key, value)
        return merged

    return reduce(merge_dicts, dict_list, {})
