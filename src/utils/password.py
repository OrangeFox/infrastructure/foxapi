import hashlib
import hmac


def check_pass_mpanel(password: str, key: bytes, salt: bytes):
    """Old MPanel's authentication method"""

    new_key = hashlib.pbkdf2_hmac(
        'sha256',
        password.encode('utf-8'),
        salt,
        100000
    )

    # Use constant-time comparison to prevent timing attacks
    return hmac.compare_digest(new_key, key)
