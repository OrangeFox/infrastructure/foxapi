import hashlib
import hmac
import secrets


def check_pass_mpanel(password: str, key: bytes, salt: bytes):
    """Old MPanel's authentication method"""

    new_key = hashlib.pbkdf2_hmac(
        'sha256',
        password.encode('utf-8'),
        salt,
        100000
    )

    # Use constant-time comparison to prevent timing attacks
    return hmac.compare_digest(new_key, key)


def generate_pass_mpanel(password: str) -> tuple[bytes, bytes]:
    """
    Generate a secure password hash for MPanel authentication method.

    Args:
        password (str): The password to hash

    Returns:
        tuple[bytes, bytes]: A tuple containing (key, salt)
    """
    # Generate a cryptographically secure random salt
    salt = secrets.token_bytes(16)

    key = hashlib.pbkdf2_hmac(
        'sha256',
        password.encode('utf-8'),
        salt,
        100000
    )

    return key, salt


def validate_password(password: str) -> str | None:
    """Validate a password for MPanel authentication method. Returns None if valid, otherwise an error message."""

    if len(password) < 8:
        return "Password must be at least 8 characters long."
    if not any(char.isdigit() for char in password):
        return "Password must include at least one number."
    if not any(char.isalpha() for char in password):
        return "Password must include at least one letter."
    if not any(char.isupper() for char in password):
        return "Password must include at least one uppercase letter."

    # Valid password
    return None
