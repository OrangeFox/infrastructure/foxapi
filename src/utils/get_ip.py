from starlette.requests import Request


def get_client_ip(request: Request) -> str:
    # First try Cloudflare's specific header
    if "CF-Connecting-IP" in request.headers:
        return request.headers["CF-Connecting-IP"]
    # Then try the standard X-Forwarded-For header
    elif "X-Forwarded-For" in request.headers:
        # X-Forwarded-For can contain multiple IPs, the client is the first one
        return request.headers["X-Forwarded-For"].split(",")[0].strip()
    # Fall back to the direct client IP
    return request.client.host
