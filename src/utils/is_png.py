def ispng(f: bytes):
    return f.startswith(b'\x89PNG\r\n\x1A\n')
