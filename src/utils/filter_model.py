from typing import Optional, Any, Type

from beanie.odm.operators.find.comparison import In
from beanie.odm.queries.find import FindMany
from fastapi import Query
from pydantic import BaseModel


class CommonQueryParams:
    def __init__(
            self,
            skip: Optional[int] = Query(0, description="Skip query results"),
            limit: Optional[int] = Query(0, description="Limit query results (0 means unlimited)")
    ):
        self.skip = skip or 0
        self.limit = limit or 0

    def filter(self, obj: FindMany[Any]) -> FindMany[Any]:
        return obj.skip(self.skip).limit(self.limit)


def filter_in(obj: FindMany[Any], model: Type[BaseModel], **kwargs) -> FindMany[Any]:
    for field_name, contains in kwargs.items():
        if not contains:
            continue
        obj = obj.find(In(getattr(model, field_name), contains))
    return obj
