import re
from datetime import datetime, timezone
from hashlib import md5
from pathlib import Path
from typing import IO, Tuple, Dict, List, Optional
from zipfile import ZipFile

from beanie.odm.operators.find.comparison import In
from beanie.odm.operators.find.logical import Or
from fastapi import UploadFile, HTTPException

from src.models.device import Device
from src.models.releasemodel import ReleaseType, RecoveryImg, ReleaseModel, ReleaseInternal
from src.settings import SETTINGS
from src.utils.object_id import ObjectId

RELEASE_FILENAME_PATTERN = re.compile(r'OrangeFox-([^\-]+)-([^\-]+)-([^\-]+)\.(.+).zip')
PROPS_TYPE = Dict[str, str]


def process_props_file(props_file: IO[bytes]) -> PROPS_TYPE:
    props = {}
    for line in props_file.read().decode('UTF-8').splitlines():
        if not line or line.startswith('#'):
            continue

        key, value = line.split('=', 1)
        props[key] = value.removeprefix('"').removesuffix('"')

    if not props:
        raise Exception('Empty props file')

    return props


def extract_props(zip_file: ZipFile) -> Tuple[PROPS_TYPE, PROPS_TYPE]:
    files = zip_file.namelist()

    if 'META-INF/debug/fox_build_vars.txt' not in files:
        raise HTTPException(
            status_code=400,
            detail='Missing fox_build_vars.txt',
        )

    if 'META-INF/debug/default.prop' not in files:
        raise HTTPException(
            status_code=400,
            detail='Missing default.prop',
        )

    build_vars = process_props_file(zip_file.open('META-INF/debug/fox_build_vars.txt'))
    props = process_props_file(zip_file.open('META-INF/debug/default.prop'))

    props = {key.replace('.', ','): value for key, value in props.items()}
    return build_vars, props


def extract_type(build_vars: PROPS_TYPE) -> ReleaseType:
    raw_string = build_vars['FOX_BUILD_TYPE']

    if raw_string == 'Stable':
        return ReleaseType.stable
    elif raw_string == 'Beta':
        return ReleaseType.beta
    elif raw_string == 'Alpha':
        return ReleaseType.alpha

    raise HTTPException(
        status_code=400,
        detail='Invalid FOX_BUILD_TYPE',
    )


async def check_release_zip(release_zip: UploadFile, expected_md5: str):
    # Check release MD5 hash
    if md5(await release_zip.read()).hexdigest() != expected_md5:
        raise HTTPException(
            status_code=400,
            detail='Invalid release MD5 hash',
        )

    # if not RELEASE_FILENAME_PATTERN.match(release_zip.filename):
    #     raise HTTPException(
    #         status_code=400,
    #         detail='Invalid release filename. Do not rename the release files',
    #     )


def recovery_img_info(zip_file: ZipFile):
    recovery_img_file = zip_file.open('recovery.img')

    size = len(recovery_img_file.read())
    recovery_img_file.seek(0)

    md5_hash = md5(recovery_img_file.read()).hexdigest()
    return RecoveryImg(size=size, md5=md5_hash)


def check_build_vars_and_props(build_vars: PROPS_TYPE, props: PROPS_TYPE):
    required_vars = ['FOX_VERSION', 'FOX_BUILD_TYPE', 'FOX_VARIANT']
    for key in required_vars:
        if key not in build_vars:
            raise HTTPException(
                status_code=400,
                detail=f'Missing {key} in build_vars',
            )

    required_props = ['ro,build,fox_id', 'ro,build,date,utc_fox', 'ro,build,fox_codebase', 'ro,build,product']
    for key in required_props:
        if key not in props:
            raise HTTPException(
                status_code=400,
                detail=f'Missing {key} in props',
            )


async def prevent_duplicate_release(release: ReleaseModel):
    # Unique build id
    if release.internal.prop and 'ro,build,fox_id' in release.internal.prop and await ReleaseModel.find_one(
            ReleaseModel.internal.prop['ro,build,fox_id'] == release.internal.prop['ro,build,fox_id']
    ):
        raise HTTPException(
            status_code=409,
            detail='Another release with the same fox_id already exists',
        )

    if await ReleaseModel.find_one(ReleaseModel.md5 == release.md5):
        raise HTTPException(
            status_code=409,
            detail='Another release with the same MD5 hash already exists',
        )

    if await ReleaseModel.find_one(ReleaseModel.filename == release.filename):
        raise HTTPException(
            status_code=409,
            detail='Another release with the same filename already exists',
        )

    if await ReleaseModel.find_one(ReleaseModel.device_id == release.device_id, ReleaseModel.type == release.type,
                                   ReleaseModel.version == release.version):
        raise HTTPException(
            status_code=409,
            detail='Another release with the same version already exists',
        )


async def prevent_incorrect_device_release(device_id: ObjectId, props: PROPS_TYPE):
    codename = props['ro,build,product']

    if not await Device.find_one(
            Device.id == device_id,
            Or(
                In(Device.codenames, [codename]),
                Device.codenames == codename
            ),
    ):
        raise HTTPException(
            status_code=400,
            detail="The release's codename doesn't match the device's one.",
        )


async def make_release(
        device_id: ObjectId,
        release_zip: UploadFile,
        md5: str,
        changelog: List[str],
        bugs: List[str],
        notes: Optional[str],
        user_id: ObjectId,
        user_roles: List[str],
        force: bool = False,
) -> ReleaseModel:
    if not force:
        await check_release_zip(release_zip, md5)

    build_vars, props = extract_props(ZipFile(release_zip.file))
    release_type = extract_type(build_vars)

    if release_type == ReleaseType.stable and 'releasing_stable' not in user_roles:
        raise HTTPException(
            status_code=403,
            detail='You are not allowed to upload stable releases',
        )

    size = release_zip.size
    recovery_img = recovery_img_info(ZipFile(release_zip.file))

    internal = ReleaseInternal(
        prop={**props, **build_vars},
        released_by=user_id
    )

    release = ReleaseModel(
        device_id=device_id,
        date=datetime.now(timezone.utc),
        type=release_type,
        variant=build_vars.get('FOX_VARIANT', None),
        version=build_vars['FOX_VERSION'],
        changelog=changelog,
        bugs=bugs or None,
        notes=notes,
        filename=release_zip.filename,
        md5=md5,
        freezed=False,
        size=size,
        recovery_img=recovery_img,
        internal=internal,
    )

    if not force:
        check_build_vars_and_props(build_vars, props)
        await prevent_duplicate_release(release)
        await prevent_incorrect_device_release(device_id, props)
    release = await release.save()

    # Save release to the disk
    real_filename = Path(f'{release.id}.zip')
    real_path = Path(SETTINGS.files_path / real_filename)
    with open(real_path, 'xb') as f:
        f.write(await release_zip.read())

    return release
