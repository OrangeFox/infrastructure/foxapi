from typing import Any, Type, Callable

from beanie import Link
from bson import ObjectId as BsonObjectId
from bson.errors import InvalidId
from pydantic_core import CoreSchema, core_schema
from pydantic_core.core_schema import ToStringSerSchema


class ObjectId(BsonObjectId):
    @classmethod
    def __get_pydantic_core_schema__(
            cls, source: Type[Any], handler: Callable[[Any], CoreSchema]
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(
            cls._validate, core_schema.any_schema(), serialization=core_schema.to_string_ser_schema()
        )

    @classmethod
    def _validate(cls, value: Any) -> BsonObjectId:
        if isinstance(value, Link):
            value = value.ref.id
        
        try:
            return ObjectId(value)
        except (InvalidId, TypeError) as e:
            raise ValueError("Not a valid ObjectId") from e
