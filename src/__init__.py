from slowapi import Limiter

from src.settings import SETTINGS
from src.utils.get_ip import get_client_ip
from src.utils.logger import log

limiter = Limiter(key_func=get_client_ip, default_limits=["40/minute" if SETTINGS.internal else "20/minute"])
