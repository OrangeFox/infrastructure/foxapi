from fastapi import FastAPI

from .device import router as device_router
from .oems import router as oems_router
from .releases import router as release_router
from .report import router as reports_router
from .stats import router as stats_router
from .updates import router as updates_router


def include_routers(app: FastAPI, intern: bool):
    app.include_router(device_router)
    app.include_router(oems_router)
    app.include_router(release_router)
    app.include_router(updates_router)
    app.include_router(reports_router)
    app.include_router(stats_router)

    if intern:
        from .internal.auth import router as auth_router
        from .internal.release import router as internal_release_router
        from .internal.device import router as internal_device_router
        from .internal.users import router as internal_users_router
        from .internal.profile import router as internal_profile_router

        app.include_router(auth_router)
        app.include_router(internal_release_router)
        app.include_router(internal_device_router)
        app.include_router(internal_users_router)
        app.include_router(internal_profile_router)
