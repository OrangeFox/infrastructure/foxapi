from typing import List, Optional
from uuid import UUID

from beanie.odm.operators.find.comparison import In, GT
from beanie.odm.operators.find.logical import Or
from fastapi import APIRouter, Depends, Query, HTTPException

from src.models.device import Device
from src.models.releasemodel import ReleaseModel, ReleaseType
from src.responses.list import ListResponse
from src.responses.release import ShortReleaseResponse, ReleaseResponse
from src.utils.filter_model import CommonQueryParams, filter_in
from src.utils.object_id import ObjectId
from src.utils.responses import responses

router = APIRouter(prefix='/releases', tags=['release'])

from enum import Enum

RELEASES_SORT_MODES = {
    'date_asc': '+date',
    'date_desc': '-date'
}


class ReleasesSort(Enum):
    date_asc = 'date_asc'
    date_desc = 'date_desc'


@router.get("/", response_model=ListResponse[ShortReleaseResponse], responses={404: responses[404]})
async def get_releases(
        query_params: CommonQueryParams = Depends(CommonQueryParams),
        release_id: List[ObjectId] = Query(None, description="Filter by Release IDs", alias='id'),
        legacy_release_id: List[ObjectId] = Query(
            None,
            description="Filter by Release IDs (deprecated)",
            deprecated=True,
            alias='_id'
        ),
        build_id: List[UUID] = Query(None, description="Filter by Build IDs"),
        device_id: List[ObjectId] = Query(None, description="Filter by Device IDs"),
        codename: List[str] = Query(None, description="Filter by device codenames", deprecated=True),
        version: List[str] = Query(None, description="Filter by release version", deprecated=True),
        release_type: List[ReleaseType] = Query(
            [ReleaseType.stable, ReleaseType.beta],
            description="Filter by release type",
            alias='type'
        ),
        archived: Optional[bool] = Query(None, description="Filter by the archived status"),
        freezed: Optional[bool] = Query(False, description="Filter by the freezed status"),
        after_release_id: Optional[ObjectId] = Query(None, description="Show releases after the provided one"),
        after_date: Optional[int] = Query(None, description="Show releases after the provided timestamp"),
        sort: ReleasesSort = Query(ReleasesSort.date_desc, description="Sort mode"),
):
    """
    Lists releases.
    
    This method contains a bunch of different parameters to filter releases for different cases.
    For example, you may use ?after_release_id param to get only releases that came after the last known one 
    (aka get updates).
    
    Getting releases by device codename is obsolete thought.
    The reason for this is that devices could change 
    codenames eventually.
    Unfortunately, some OEMs and community make a total mess of them.
    Please use /device methods to determine the right device (preferably with the user decision), save the device ID 
    somewhere and use it afterward.
    
    Version tag is deprecated for the same reason, it's not really consistent, as it might contain patch and mod 
    versions and very different by each maintainer.
    """
    find_obj = ReleaseModel.find_many()
    filtered = query_params.filter(find_obj)

    if release_id or legacy_release_id:
        ids = (release_id or []) + (legacy_release_id or [])
        filtered = filtered.find(
            In(ReleaseModel.id, ids)
        )

    device_id = device_id or []

    if build_id:
        filtered = filtered.find(In(ReleaseModel.internal.prop['ro,build,fox_id'], (str(x) for x in build_id)))

    if codename:
        async for device in Device.find(Or(
                In(Device.codenames, codename),
                In(Device.codename, codename)
        )):
            device_id.append(ObjectId(device.id))

    if after_release_id:
        filtered = filtered.find(
            GT(ReleaseModel.id, after_release_id)
        )
    if after_date:
        filtered = filtered.find(
            GT(ReleaseModel.date, after_date)
        )

    if archived:
        filtered = filtered.find(
            ReleaseModel.archived == archived
        )

    if freezed:
        filtered = filtered.find(
            ReleaseModel.freezed == freezed
        )

    filtered = filter_in(
        filtered,
        ReleaseModel,
        version=version,
        device_id=device_id,
        type=release_type,
    ).sort(RELEASES_SORT_MODES[sort.value])

    releases = await filtered.to_list()

    if not releases:
        raise HTTPException(status_code=404)

    return ListResponse.new(ShortReleaseResponse.from_db_list(releases))


@router.get('/get', response_model=ReleaseResponse)
async def get_release_extended(
        release_id: Optional[ObjectId] = Query(None, description="Release ID"),
        legacy_device_id: Optional[ObjectId] = Query(
            None,
            description="Release ID (deprecated)",
            deprecated=True,
            alias='_id'
        ),
        build_id: Optional[UUID] = Query(None, description="Build ID, `ro.build.fox_id` prop"),
        filename: Optional[str] = Query(None, deprecated=True)
):
    """
    Extended get device method, including the ability to get a device using device codename instead of the ID and
    Build ID.
    
    Example: /device/get?codename=lavender

    Calling this method without parameters will return the latest available release in the database
    
    Warning: the filename identification is deprecated! Use /release/<release_id> instead!
    """

    if legacy_device_id or release_id:
        release = await ReleaseModel.get(legacy_device_id or release_id)
    elif filename:
        release = await ReleaseModel.find_one(ReleaseModel.filename == filename)
    elif build_id:
        release = await ReleaseModel.find_one(ReleaseModel.internal.prop['ro,build,fox_id'] == str(build_id))
    else:
        # Return the latest release
        release = await ReleaseModel.find().sort(-ReleaseModel.id).first_or_none()

    if not release:
        raise HTTPException(status_code=404)
    return ReleaseResponse.from_db(release)


@router.get('/{release_id}', response_model=ReleaseResponse)
async def get_release(release_id: ObjectId):
    """
    Gets release information using the release ID, nothing to discuss really.
    """

    release = await ReleaseModel.get(release_id)
    if not release:
        raise HTTPException(status_code=404)
    return ReleaseResponse.from_db(release)
