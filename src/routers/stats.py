from fastapi import APIRouter
from starlette.requests import Request

from src import limiter
from src.models.aggregations.global_stats import GlobalStatsAggregationModel, GLOBAL_STATS_AGGREGATION
from src.models.stats import ReleaseStats
from src.utils.responses import responses

router = APIRouter(prefix='/stats', tags=['stats'])


@router.get('/', response_model=GlobalStatsAggregationModel, responses={429: responses[429]})
@limiter.limit("3/minute")
async def public_stats(request: Request):
    """
    Returns the global OrangeFox downloads statistics.
    This request causes an increased load to the database, as it needs to aggregate all the release stats data,
    therefore, it is limited to a small number of requests per minute for public usage.
    """

    pipeline = ReleaseStats.find_all().aggregate(GLOBAL_STATS_AGGREGATION,
                                                 projection_model=GlobalStatsAggregationModel)
    item = (await pipeline.to_list(length=1))[0]
    return item
