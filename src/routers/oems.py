from fastapi import APIRouter

from src.models.device import Device
from src.responses.list import ListResponse

router = APIRouter(prefix='/oems', tags=['device'])


@router.get('/', response_model=ListResponse[str])
async def get_oems_list():
    """Lists all OEM names"""    
    oems = await Device.distinct('oem_name')
    return ListResponse.new(oems)
