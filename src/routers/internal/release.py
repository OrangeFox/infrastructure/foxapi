from typing import Annotated, List, Optional

from beanie.odm.operators.update.general import Set
from fastapi import APIRouter, Depends, UploadFile, Body, Form, HTTPException

from src.middlewares.require_role import require_role
from src.models.logging import LoggingModel
from src.models.releasemodel import ReleaseModel
from src.models.stats import ReleaseStats
from src.models.user import Roles, User
from src.responses.internal.release import ReleaseInternalResponse
from src.responses.release import ReleaseResponse
from src.types.ReleaseUpdateBody import ReleaseUpdateBody
from src.utils.mod_release import delete_release
from src.utils.new_release import make_release
from src.utils.object_id import ObjectId

router = APIRouter(
    prefix='/internal/releases',
    dependencies=[Depends(require_role(Roles.maintainer))],
    tags=['internal']
)


@router.post('/')
async def new_release(
        user: Annotated[User, Depends(require_role(Roles.maintainer))],

        device_id: Annotated[ObjectId, Form()],
        release_zip: UploadFile,
        md5: Annotated[str, Form()],
        changelog: Annotated[List[str], Form()],
        bugs: List[str] = Form(None),
        notes: Optional[str] = Form(None),
        force: bool = Form(False),
) -> ReleaseResponse:
    if force and Roles.danger_tasks.value not in user['roles']:
        raise HTTPException(status_code=403, detail='You are not allowed to force release!')

    """Upload a new release manually"""
    new_release_model = await make_release(
        device_id=device_id,
        release_zip=release_zip,
        md5=md5,
        changelog=changelog,
        bugs=bugs,
        notes=notes,
        user_id=user.id,
        user_roles=user['roles'],
        force=force,
    )

    await LoggingModel.add_to_log(
        user.id,
        'new_release',
        device_id=device_id,
        release_id=new_release_model.id,
    )

    return ReleaseResponse.from_db(new_release_model)


@router.get('/{release_id}')
async def get_release_info(release_id: ObjectId) -> ReleaseInternalResponse:
    """Get release information"""
    release = await ReleaseModel.get(release_id)
    if not release:
        raise HTTPException(status_code=404)
    release_stats = await ReleaseStats.get_by_release_id(release.id)

    return ReleaseInternalResponse.from_db(release, stats=release_stats)


@router.get('/{release_id}/dl')
async def get_release_zip(release_id: ObjectId) -> bool:
    """Download release ZIP directly"""
    release = await ReleaseModel.get(release_id)
    if not release:
        raise HTTPException(status_code=404)

    pass


@router.put('/{release_id}')
async def edit_release(
        user: Annotated[User, Depends(require_role(Roles.maintainer))],
        release_id: ObjectId,
        update: ReleaseUpdateBody = Body(...),
) -> bool:
    """Edit Release information"""
    release = await ReleaseModel.get(release_id)
    if not release:
        raise HTTPException(status_code=404)

    update_data = update.dict(exclude_none=True)

    await LoggingModel.add_to_log(
        user.id,
        'edit_release',
        release_id=release.id,
        data={'changes': update_data},
    )

    await release.update(Set(update_data))

    return True


@router.delete('/{release_id}')
async def del_release(
        user: Annotated[User, Depends(require_role(Roles.maintainer))],
        release_id: ObjectId
) -> bool:
    """Delete release"""
    release = await ReleaseModel.get(release_id)
    if not release:
        raise HTTPException(status_code=404)

    await delete_release(release.id)

    await LoggingModel.add_to_log(
        user.id,
        'del_release',
        release_id=release.id,
    )

    return True
