from typing import Annotated

from beanie.odm.operators.update.general import Set
from fastapi import APIRouter, HTTPException, Depends, Body

from src.middlewares.require_role import require_role
from src.models.device import Device
from src.models.logging import LoggingModel
from src.models.user import User, Roles
from src.responses.internal.users import UserShortResponse, UserInternalResponse
from src.responses.list import ListResponse
from src.types.UserUpdateBody import UserUpdateBody
from src.utils.object_id import ObjectId

router = APIRouter(
    prefix='/internal/users',
    dependencies=[Depends(require_role(Roles.maintainer))],
    tags=['internal']
)


@router.get('/', response_model=ListResponse[UserShortResponse])
async def list_users():
    users = User.find_all()

    return ListResponse.new(await users.to_list())


@router.get('/{user_id}', response_model=UserInternalResponse)
async def get_user_by_id(user_id: str):
    user = await User.get(ObjectId(user_id))
    if not user:
        raise HTTPException(status_code=404)

    # Maintained devices
    maintained_devices = await Device.find_many(Device.maintainer == user.id).to_list()

    return {**user.model_dump(), 'maintained_devices': maintained_devices}


@router.delete('/{user_id}')
async def delete_user(
        user: Annotated[User, Depends(require_role(Roles.maintainer))],
        user_id: ObjectId
) -> bool:
    wanted_user = await User.get(user_id)
    if not wanted_user:
        raise HTTPException(status_code=404)

    await wanted_user.delete()
    await LoggingModel.add_to_log(
        user.id,
        'del_user',
        user_id=wanted_user.id,
    )
    return True


@router.put('/{user_id}', dependencies=[Depends(require_role(Roles.admin))])
async def update_user(
        user: Annotated[User, Depends(require_role(Roles.maintainer))],
        user_id: ObjectId,
        update: UserUpdateBody = Body(...)
) -> bool:
    wanted_user = await User.get(user_id)

    update_data = update.model_dump(exclude_none=True)

    await wanted_user.update(Set(update_data))

    await LoggingModel.add_to_log(
        user.id,
        'update_user',
        user_id=wanted_user.id,
        data={'changes': update_data}
    )

    return True
