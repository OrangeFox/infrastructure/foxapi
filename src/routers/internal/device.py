from typing import Annotated

from beanie.odm.operators.update.general import Set
from fastapi import APIRouter, Depends, HTTPException, Body

from src.middlewares.require_role import require_role
from src.models.device import Device
from src.models.logging import LoggingModel
from src.models.stats import ReleaseStats
from src.models.user import Roles, User
from src.responses.internal.device import DeviceInternalResponse
from src.responses.stats import DeviceStatsResponse
from src.types.DeviceUpdateBody import DeviceUpdateBody
from src.types.NewDeviceBody import NewDeviceBody
from src.utils.object_id import ObjectId

router = APIRouter(
    prefix='/internal/devices',
    dependencies=[Depends(require_role(Roles.maintainer))],
    tags=['internal']
)


@router.get('/{device_id}')
async def get_device_info(device_id: ObjectId) -> DeviceInternalResponse:
    device = await Device.get(device_id)
    if not device:
        raise HTTPException(status_code=404)

    stats = DeviceStatsResponse.from_releases_stats(await ReleaseStats.get_by_device_id(device_id))
    return await DeviceInternalResponse.from_db(device, stats)


@router.post('/')
async def new_device(
        user: Annotated[User, Depends(require_role(Roles.create_new_devices))],
        update_body: NewDeviceBody = Body(...),
) -> str:
    device = Device(
        codename=update_body.codenames[0],
        model_name=update_body.model_names[0],

        supported=True,
        freezed=False,
        **update_body.model_dump()
    )

    await device.insert()
    await LoggingModel.add_to_log(
        user.id,
        'create_new_device',
        device_id=device.id
    )
    return str(device.id)


@router.put('/{device_id}')
async def update_device(
        user: Annotated[User, Depends(require_role(Roles.maintainer))],
        device_id: ObjectId,
        update: DeviceUpdateBody = Body(...),
) -> bool:
    device = await Device.get(device_id)
    if not device:
        raise HTTPException(status_code=404, detail="Device not found")

    update_data = update.dict(exclude_none=True)
    if 'model_names' in update_data:
        update_data['model_name'] = update_data['model_names'][0]
    if 'codenames' in update_data:
        update_data['codename'] = update_data['codenames'][0]

    await LoggingModel.add_to_log(
        user.id,
        'edit_device',
        device_id=device.id,
        data={'changes': update_data}
    )

    await device.update(Set(update_data))

    return True
