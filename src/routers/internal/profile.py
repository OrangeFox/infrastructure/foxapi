from typing import Annotated

from beanie.odm.operators.update.general import Set
from fastapi import Depends, APIRouter, Body
from starlette.exceptions import HTTPException

from src.middlewares.require_role import require_role
from src.models.user import Roles, User
from src.utils.password import generate_pass_mpanel, validate_password, check_pass_mpanel

router = APIRouter(
    prefix='/profile',
    dependencies=[Depends(require_role(Roles.maintainer))],
    tags=['internal']
)


@router.post("/change_password")
async def change_password(
        user: Annotated[User, Depends(require_role(Roles.maintainer))],
        old_password: str = Body(),
        new_password: str = Body(),
) -> bool:
    """
    Change the password for a user.
    """

    if not check_pass_mpanel(old_password, user.key, user.salt):
        raise HTTPException(
            status_code=401,
            detail="The old password is incorrect.",
        )

    validation_error = validate_password(new_password)
    if validation_error:
        raise HTTPException(status_code=400, detail=validation_error)

    key, salt = generate_pass_mpanel(new_password)

    await user.update(Set({'key': key, 'salt': salt}))

    return True
