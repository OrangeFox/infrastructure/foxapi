from datetime import datetime, timedelta, UTC
from typing import Annotated

from fastapi import APIRouter, HTTPException, Body, Request, Depends
from jwt import encode, decode, ExpiredSignatureError

from src import limiter
from src.middlewares.randomized_response_time import randomized_response_time
from src.models.logging import LoggingModel
from src.models.token import Token
from src.models.user import User
from src.settings import SETTINGS
from src.utils.get_ip import get_client_ip
from src.utils.logger import log
from src.utils.password import check_pass_mpanel

router = APIRouter(prefix='/auth', tags=['internal'])
JWT_ALGORITHMS = ["HS256"]


async def new_token(user: User) -> str:
    time_now = datetime.now(tz=UTC)
    payload = {
        'user_id': str(user.id),
        'username': user.username,
        'name': user.name,
        'roles': user.roles,
        'iat': time_now.timestamp(),
        'exp': (time_now + timedelta(seconds=SETTINGS.token_expire)).timestamp(),
    }

    encoded = encode(payload, SETTINGS.secret, algorithm=JWT_ALGORITHMS[0])
    token = Token(**payload, token=encoded)

    await token.create()
    log.info("New token was given", username=user.username)

    return encoded


@router.post('/login')
@limiter.limit("20/hour")
async def auth(
        username: Annotated[str, Body()],
        password: Annotated[str, Body()],
        request: Request,
        _delay: None = Depends(randomized_response_time)
) -> str:
    corrected_username = username.strip().rstrip()

    user = await User.find_one(User.username == corrected_username)
    if not user:
        raise HTTPException(status_code=401, detail="Unauthorized")

    elif not user.key or not user.salt:
        raise HTTPException(status_code=401, detail="Unauthorized")
    elif not check_pass_mpanel(password, user.key, user.salt):
        raise HTTPException(status_code=401, detail="Unauthorized")

    # Retrieve all tokens
    time_now = datetime.now(tz=UTC)
    async for token in Token.find(Token.username == corrected_username):
        expire_at = token.exp.replace(tzinfo=UTC)

        if expire_at < time_now:
            await token.delete()
        else:
            return token.token

    await LoggingModel.add_to_log(
        user.id,
        'login',
        ip=get_client_ip(request)
    )

    return await new_token(user)


@router.post('/renew')
@limiter.limit("40/hour")
async def renew(token: Annotated[str, Body()], request: Request) -> str:
    # Find the last token
    token_model = await Token.find_one(Token.token == token)
    if not token_model:
        raise HTTPException(status_code=403, detail="Forbidden")

    try:
        decoded_token = decode(token, SETTINGS.secret, algorithms=JWT_ALGORITHMS)
    except ExpiredSignatureError:
        raise HTTPException(status_code=403, detail="Forbidden")

    if decoded_token['user_id'] != str(token_model.user_id.ref.id):
        raise HTTPException(status_code=403, detail="Forbidden")

    user = await token_model.user_id.fetch()

    time_now = datetime.now(tz=UTC)

    expire_at = token_model.exp.replace(tzinfo=UTC)
    if expire_at < time_now:
        await token_model.delete()
        raise HTTPException(status_code=403, detail="Forbidden")

    half_expire_time = expire_at - timedelta(seconds=SETTINGS.token_expire / 2)
    if time_now < half_expire_time:
        return token

    # Delete all older tokens
    await Token.find_all(Token.user_id == token_model.user_id).delete()

    await LoggingModel.add_to_log(
        user.id,
        'token_renew',
        ip=get_client_ip(request)
    )

    return await new_token(user)


@router.post('/logout')
async def logout(token: Annotated[str, Body()], request: Request):
    token_model = await Token.find_one(Token.token == token)
    if not token_model:
        raise HTTPException(status_code=404, detail="No such token found")

    await token_model.delete()

    await LoggingModel.add_to_log(
        token_model.user_id,
        'token_renew',
        ip=get_client_ip(request)
    )

    return "OK"
