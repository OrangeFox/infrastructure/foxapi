from typing import Optional, List

from beanie.odm.operators.find.comparison import In
from beanie.odm.operators.find.logical import Or
from fastapi import APIRouter, Depends, HTTPException, Query

from src.models.device import Device
from src.responses.device import DeviceResponse, ShortDeviceResponse
from src.responses.list import ListResponse
from src.utils.filter_model import filter_in, CommonQueryParams
from src.utils.object_id import ObjectId
from src.utils.responses import responses

router = APIRouter(prefix='/devices', tags=['device'])


@router.get('/', response_model=ListResponse[ShortDeviceResponse], responses={404: responses[404]})
async def get_devices_list(
        query_params: CommonQueryParams = Depends(CommonQueryParams),
        device_id: List[ObjectId] = Query(None, description="Filter by Device IDs", alias='id'),
        legacy_device_id: List[ObjectId] = Query(
            None,
            description="Filter by Device IDs (deprecated)",
            deprecated=True,
            alias='_id'
        ),
        oem_name: List[str] = Query(None, description="Filter by OEM names"),
        codename: List[str] = Query(None, description="Filter by device codenames"),
        model_name: List[str] = Query(None, description="Filter by model names", deprecated=True),
        supported: Optional[bool] = Query(None, description="Supported status"),
        maintainer: List[str] = Query(None, description="Filter by maintainer ID"),
        freezed: Optional[bool] = Query(False, description="Filter by Freezed status"),
):
    """
    Get a device list.
    
    Device ID is the internal OrangeFox device identifier
    (https://www.mongodb.com/docs/manual/reference/method/ObjectId/).
    Please only use it for identification instead of codename or model_name if it's possible.
    You would find two Device ID query parameters here, the _id one is left deprecated due to historical reasons, 
    please use id instead.
    
    Please note that using model_name is unrecommended and considered deprecated as it's very inconsistent around some 
    OEMs that play with the names around.
    As of the current moment, the API won't support unified model names at all!
    In short, do not use model_name as device's identification.
    
    Be careful using codenames, there are a lot of problems and pain about those.
    Some OEMs keep releasing new phones
    with the same hardware as the old ones and may or may not update or change the codename for those.
    Currently, API doesn't support unified devices; they would have only one codename.
    
    Our OEMs database would not include subbrands, they instead would be added to the model name.
    Like oem_name: "Xiaomi" model_name: "Poco F5 Pro".
    The exclusions are merged ones, like OnePlus, as calling those BBK OnePlus is really weird in the respective 
    communities.
     
    Note: You will get only a shortened device object with this method, if you want to get a full one,
    use "/device/<device_id>" or "/device/get" method instead.
    """
    find_obj = Device.find_many()
    filtered = query_params.filter(find_obj)
    if device_id or legacy_device_id:
        ids = (device_id or []) + (legacy_device_id or [])
        filtered = filtered.find(
            In(Device.id, ids)
        )

    if codename:
        filtered = filtered.find(Or(
            In(Device.codenames, codename),
            In(Device.codename, codename)
        ))

    if model_name:
        filtered = filtered.find(Or(
            In(Device.model_names, model_name),
            In(Device.model_name, model_name)
        ))

    if supported:
        filtered = filtered.find(Device.supported == supported)

    if freezed:
        filtered = filtered.find(Device.freezed == freezed)

    filtered = filter_in(
        filtered,
        Device,
        oem_name=oem_name,
        maintainer=(ObjectId(x) for x in maintainer) if maintainer else None,
    )

    devices = await filtered.to_list()

    if not devices:
        raise HTTPException(status_code=404)

    return ListResponse.new(devices)


@router.get('/get', response_model=DeviceResponse,
            responses={404: responses[404], 400: responses[400]})
async def get_device_extended(
        device_id: Optional[ObjectId] = None,
        legacy_device_id: Optional[ObjectId] = Query(
            None,
            description="Filter by Device ID (deprecated)",
            deprecated=True,
            alias='_id'
        ),
        codename: Optional[str] = Query(None, description="Not recommended to use when you can")
):
    """
    Extended get device method, including the ability to get a device using device codename instead of the ID.\n
    /device/get?codename=lavender\n
    
    Due to the reasons mentioned in the /device route, identification devices using their codenames are considered 
    unrecommended.
    """

    if legacy_device_id or device_id:
        device = await Device.get(legacy_device_id or device_id)
    elif codename:
        device = await Device.get_by_codename(codename)
    else:
        # Last device
        device = await Device.find().sort(-Device.id).first_or_none()

    if not device:
        raise HTTPException(status_code=404)
    return await DeviceResponse.from_db(device)


@router.get('/{device_id}', response_model=DeviceResponse, responses={404: responses[404]})
async def get_device(device_id: str):
    """Gets device info using device ID."""

    device = await Device.get(device_id)
    if not device:
        raise HTTPException(status_code=404)
    return await DeviceResponse.from_db(device)
