from base64 import b64decode
from typing import Annotated, List, Dict, Optional

from fastapi import APIRouter, File, Form, HTTPException
from pydantic import BaseModel, Field

from src.models.report import Report
from src.settings import SETTINGS
from src.utils.is_png import ispng
from src.utils.object_id import ObjectId

# include_in_schema: show only when internal mode is enabled, but itll work even if it's disabled
router = APIRouter(prefix='/report', tags=['report'], include_in_schema=SETTINGS.internal)


class NewReportRequest(BaseModel):
    details: str = Field(max_length=200)
    screenshots: Annotated[List[str], File(max_items=5, max_length=200000)]
    details: Annotated[str, Form(max_length=200)]
    logs: Annotated[Dict[str, str], Form(max_length=200000)]
    dmesg: Annotated[Optional[str], Form(max_length=200000)]
    logcat: Annotated[Optional[str], Form(max_length=200000)]


@router.post('/{release_id}')
async def new_report(
        release_id: ObjectId,
        item: NewReportRequest
) -> str:
    """
    This method is currently not available for the public usage!
    """

    # Convert base64 to binary for screenshots
    converted_screenshots = [b64decode(x) for x in item.screenshots]
    if any(not ispng(x) for x in converted_screenshots):
        raise HTTPException(status_code=400, detail='Invalid screenshot format, expected .png')

    logs_merged: Dict[str, str] = item.logs
    if item.logcat:
        logs_merged['logcat'] = item.logcat
    if item.dmesg:
        logs_merged['dmesg'] = item.dmesg

    report = Report(
        release_id=release_id,  # type: ignore
        details=item.details,
        screenshots=converted_screenshots,
        logs=logs_merged,
    )
    report = await report.create()
    return str(report.id)
