from typing import List

from beanie.odm.operators.find.comparison import GT
from fastapi import APIRouter, Query, Depends
from starlette.requests import Request

from src import limiter
from src.models.releasemodel import ReleaseType, ReleaseModel
from src.responses.list import ListResponse
from src.responses.release import ShortReleaseResponse
from src.routers.releases import RELEASES_SORT_MODES, ReleasesSort
from src.utils.filter_model import filter_in, CommonQueryParams
from src.utils.object_id import ObjectId

router = APIRouter(prefix='/updates', tags=['release'], deprecated=True)


@router.get("/{last_known_id}", response_model=ListResponse[ShortReleaseResponse])
@limiter.limit("100/minute")
async def get_updates(
        request: Request,
        last_known_id: ObjectId,
        query_params: CommonQueryParams = Depends(CommonQueryParams),
        device_id: List[ObjectId] = Query(None, description="Filter by device ID"),
        release_type: List[ReleaseType] = Query(
            [ReleaseType.stable, ReleaseType.beta],
            description="Filter by release type",
            alias='type'
        ),
        legacy_release_type: List[ReleaseType] = Query(
            None,
            description="Filter by release type (legacy)",
            alias='release_type'
        ),
):
    """
    Get updates method.
    Returns all new releases with release IDs.

    For example, "/updates/<id>" will return a list of all releases that were created after.
    You can also use "device_id" or / and "release_type" params. The first one will filter releases by specific
    devices, the second one will filter by release type. This can be useful, for example,
    if you want to notify users only with stable releases for this specific device.

    Warning: This method is obsolete! Please use /release/?after_release_id= instead!
    """

    find_obj = ReleaseModel.find_many()
    filtered = query_params.filter(find_obj)

    filtered = filtered.find(
        GT(ReleaseModel.id, last_known_id)
    )

    filtered = filter_in(
        filtered,
        ReleaseModel,
        device_id=device_id,
        type=legacy_release_type or release_type,
    ).sort(RELEASES_SORT_MODES[ReleasesSort.date_asc.value])
    return ListResponse.new(await filtered.to_list())
