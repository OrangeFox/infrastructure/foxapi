FROM python:3.12-slim as builder

ENV PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1

RUN apt update
RUN apt install build-essential gcc -y
RUN pip install poetry wheel
RUN python -m venv /venv

RUN poetry self add poetry-plugin-export

COPY pyproject.toml poetry.lock ./

RUN poetry export -f requirements.txt --without-hashes | /venv/bin/pip install -r /dev/stdin

FROM python:3.12-slim as final

ENV PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1

COPY --from=builder /venv /venv
COPY src /src/

CMD [ "/venv/bin/python", "-m", "src" ]
